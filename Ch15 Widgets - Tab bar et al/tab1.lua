--
-- Project: tabIcon.png
-- Description: 
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- 
-- 
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()-- Our scene
function scene:createScene( event )
	local group = self.view
			-- Display a background
	local background = display.newImage( "background.png", true )	group:insert( background )
	
	-- Status text box
	local statusBox = display.newRect( 70, 290, 210, 120 )
	statusBox:setFillColor( 0, 0, 0 )
	statusBox.alpha = 0.4
	group:insert( statusBox )			-- Status text
	local statusText = display.newText( "Interact with a widget to begin!", 80, 300, 200, 0, native.systemFont, 20 )
	statusText.x = statusBox.x
	statusText.y = statusBox.y - ( statusBox.contentHeight * 0.5 ) + ( statusText.contentHeight * 0.5 )
	group:insert( statusText )			---------------------------------------------------------------------------------------------
	-- widget.newSegmentedControl()
	---------------------------------------------------------------------------------------------
	
	-- The listener for our segmented control
	local function segmentedControlListener( event )
		local target = event.target
		
		-- Update the status box text
		statusText.text = "Segmented Control\nSegment Pressed: " .. target.segmentLabel
		
		-- Update the status box text position
		statusText.x = statusBox.x
		statusText.y = statusBox.y - ( statusBox.contentHeight * 0.5 ) + ( statusText.contentHeight * 0.5 )
	end
	
	-- Create a default segmented control (using widget.setTheme)
	local segmentedControl = widget.newSegmentedControl
	{
	    left = 10,
	    top = 60,
	    segments = { "Aren't", "Segment", "Control", "Widgets", "Fun?" },
	    defaultSegment = 1,
	    onPress = segmentedControlListener,
	}
	group:insert( segmentedControl )	end

scene:addEventListener( "createScene" )

return scene