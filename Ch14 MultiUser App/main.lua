


local physics = require "physics"
require "pubnub"physics.start()physics.setGravity(0,0)-- create box to move on screenlocal box = display.newImage("button1.png")box.x = display.contentWidth/2box.y = display.contentHeight/2physics.addBody(box, {friction = 0, bounce = 0, density = 0})-- intialize pubnub networkingmultiplayer = pubnub.new({       publish_key     = "demo",       subscribe_key = "demo",       secret_key       = nil,        ssl                    = nil,        origin              = "pubsub.pubnub.com"})multiplayer:subscribe({
    channel = "Ch14-MultiUserDemo",
    callback = function(message)
        print("Received: "..message.msgtexta..", "..message.msgtextb)        transition.to(box, {x=message.msgtexta, y=message.msgtextb, timer = 500})
    end,
    errorback = function()
        print("Oh no!!! Dropped 3G Conection!")
    end
})

function send_a_message(imageX, imageY)
    multiplayer:publish({
        channel = "Ch14-MultiUserDemo",
        message = { msgtexta = imageX, msgtextb=imageY }
    })
end

function update_coord()
    send_a_message(box.x, box.y)
end
local function startDrag( event )
	local t = event.target

	local phase = event.phase
	if "began" == phase then
		display.getCurrentStage():setFocus(t)
		t.isFocus = true
		
		--Store inital position
		t.x0 = event.x - t.x
		t.y0 = event.y - t.y
		
		-- make the body type 'kinematic' to avoid gravity problems
		event.target.bodyType = "kinematic"
		
		-- stop current motion
		event.target:setLinearVelocity( 0,0)
		event.target.angularVelocity = 0
		
	elseif t.isFocus then
		if "moved" == phase then
			t.x = event.x - t.x0
			t.y = event.y - t.y0
		elseif "ended" == phase or "cancelled" == phase then
			display.getCurrentStage():setFocus(nil)
			t.isFocus = false
			
			-- switch body type back to "dynamic"
			if (not event.target.isPlatform) then
				event.target.bodyType = "dynamic"
			end
		end
	end
	return true
end
timer.performWithDelay( 1000, update_coord, 100 )


box:addEventListener("touch", startDrag)

