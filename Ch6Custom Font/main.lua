-- Project: Ch6CustomFont
-- Description:
--
-- Version: 1.0
-- Managed with http://OutlawGameTools.com
--
-- Copyright 2013 Brian Burton. All Rights Reserved.
---- cpmgen main.lua

local fonts = native.getFontNames()



-- Display each font in the terminal console
for i, fontname in ipairs(fonts) do
        print( "fontname = " .. tostring( fontname ) )
end


display.newText("Hello World",10, 10, "Baroque Script", 24)

