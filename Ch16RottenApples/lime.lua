-- Project: Lime Lite
--
-- Date: 09-Feb-2011
--
-- Version: 3.3
--
-- File name: lime.lua
--
-- Author: Graham Ranson 
--
-- Support: www.justaddli.me
--
-- Copyright (C) 2011 MonkeyDead Studios Limited. All Rights Reserved.

--- The main Lime manager source file.
--  The API's in this source file can be seen as a set of manager methods for using Lime.

----------------------------------------------------------------------------------------------------
----									POLITE NOTICE / PSA										----
----------------------------------------------------------------------------------------------------
----																							----
----	I have put a lot of work into this library and plan to support it for a very, very		---- 
----	long time so please don't give the code to anyone else. I doubt that anyone would as	----
----	we are all developers in the same boat but I just thougt I would put this here in 		----
----	case any one wondered if it was ok to share.											----
----																							----
----	If you did get this code through less than legitimate means then please consider		----
----	buying it legally, it is (I think) affordably priced and you will get free updates		----
----	and support	for life.																	----
----																							----
----	I hope you enjoy using it as much as I have enjoyed writing it and I also hope that		----
----    you will support me and the development of Lime by telling your friends about it etc	----
----	although naturally I don't require any link backs of any kind, it is completely up		----
----	to you.																					----
----																							----
----	If you have any additions or fixes that you would like included in the main releases	----
----	please contact me via the forums or email - graham@grahamranson.co.uk					----
----																							----
----------------------------------------------------------------------------------------------------

module(..., package.seeall)

----------------------------------------------------------------------------------------------------
----									LIME MODULES											----
----------------------------------------------------------------------------------------------------

utils = require("lime-utils")

require("lime-external-xml_parser")
require("lime-external-Json")

require("lime-map")
require("lime-tileLayer")
require("lime-objectLayer")
require("lime-tile")
require("lime-object")
require("lime-tileSet")
require("lime-property")

----------------------------------------------------------------------------------------------------
----									MODULE VARIABLES										----
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
----									GLOBAL (YIK) PROPERTIES									----
----------------------------------------------------------------------------------------------------

_G.limeScreenCullingEnabled = false

----------------------------------------------------------------------------------------------------
----									PUBLIC PROPERTIES										----
----------------------------------------------------------------------------------------------------

isSimulator = system.getInfo("environment") == "simulator"

----------------------------------------------------------------------------------------------------
----									PUBLIC METHODS										    ----
----------------------------------------------------------------------------------------------------

--- Enables debug mode so messages are printed to the console when things happen.
function enableDebugMode()
	limeDebugModeEnabled = true
end

--- Disables debug mode so messages aren't printed to the console. Errors will still be printed.
function disableDebugMode()
	limeDebugModeEnabled = false
end	

--- Checks if debug mode is currently enabled or disabled.
-- @return True if enabled, false if not.
function isDebugModeEnabled()
	return limeDebugModeEnabled
end

--- Loads a map.
-- @param fileName The filename of the map.
-- @param baseDirectory Path to load the map data from filename. Default is system.ResourceDirectory.
---- @return The loaded Map object.
function loadMap(fileName, baseDirectory)
	return Map:new(fileName, baseDirectory)	
end

--- Creates the visual representation of a map.
-- @param map The map to create.
-- @return The display group for the map world.
function createVisual(map)
	return map:create()
end

--- Creates the visual representation of a single layer.
-- @param map The map that contains the layer.
-- @param layerName The name of the layer.
-- @return The created layer.
function createTileLayer(map, layerName)
	local layer = map:getTileLayer(layerName)
	
	if layer then
		return layer:create()
	end
end

--- Creates the visual debug representation of a single object layer.
-- @param map The map that contains the object layer.
-- @param layerName The name of the object layer.
-- @return The created object layer.
function createObjectLayer(map, layerName)
	local layer = map:getObjectLayer(layerName)
	
	if layer then
		return layer:create()
	end
end

--- Build a physical representation of a map.
-- @param map - The map to build. 
function buildPhysical(map)
	map:build()
end

--- Enable screen space culling.
function enableScreenCulling()
	_G.limeScreenCullingEnabled = true
end

--- Disable screen space culling.
function disableScreenCulling()
	_G.limeScreenCullingEnabled = false
end	

--- Check if screen culling is enabled.
-- @return True if enabled, false if not.
function isScreenCullingEnabled()
	return _G.limeScreenCullingEnabled
end