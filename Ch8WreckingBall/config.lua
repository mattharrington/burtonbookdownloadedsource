-- config.lua for project: Ch8WreckingBall
-- Managed with http://CoronaProjectManager.com
-- Copyright 2011 Brian Burton. All Rights Reserved.
-- cpmgen config.lua
application =
{
	content =
	{
		width = 400,
		height = 800,
		scale = "letterbox",
		fps = 30,
		antialias = false,
		xalign = "center",
		yalign = "center"
	}
}