-- Project: Ch11.0 Widgets
-- Description:
--
-- Version: 1.0
-- Managed with http://OutlawGameTools.com
--
-- Copyright 2013 Brian Burton. All Rights Reserved.
---- cpmgen main.lua

local widget = require( "widget" )

-- Function to handle button events
local function handleButtonEvent( event )
    local phase = event.phase 

    if "ended" == phase then
        print( "You pressed and released a button!" )
    end
end

-- Create the button
local myButton = widget.newButton
{
    left = 100,
    top = 200,
    width = 150,
    height = 50,
    defaultFile = "default.png",
    overFile = "over.png",
    id = "button_1",
    label = "Button",
    onEvent = handleButtonEvent,
}


