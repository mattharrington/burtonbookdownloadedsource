

--====================================================================--
-- SCENE: displayStudent
--====================================================================--

--[[

******************
 - INFORMATION
******************

  - Display class information to screen.

--]]
	local widget = require ( "widget" )
	local storyboard = require("storyboard")
	local scene = storyboard.newScene()
	local localGroup = display.newGroup()
	--include SQLite
    require "sqlite3"
	

--Called if the scene hasn't been previously seen
function scene:createScene( event )
	
	-- open database	
	local path = system.pathForFile("students.sqlite", system.DocumentsDirectory)
	db = sqlite3.open( path ) 
	print(path)
		
		
	--print all the table contents
	local sql = "SELECT * FROM myclass"
	for row in db:nrows(sql) do
  	local text = row.FullName.." "..row.SID..", "..row.ClassSeat.." "..row.Grade
  	local t = display.newText(text, 20, 30 * row.id, native.systemFont, 24)

  	t:setTextColor(255,255,255)
  	localGroup:insert(t)
	end	
	
	db:close()
	
	-- Setup function for button to load student data
	local displayClass_function = function( event )
			-- return to menu screen

			storyboard.gotoScene( "menu", "slideRight",400 )
	end
	
	local  displayClass_button = widget.newButton{
					defaultFile = "selectButton.png",
					overFile = "selectButton.png",
				    label = " Return to Menu",
				    size = 24,
				    emboss=true,
					onRelease = displayClass_function,
					id = "displayClass"
	}
	displayClass_button.x = display.contentWidth/2+100
	displayClass_button.y = display.contentHeight-200
	
	-- add all display items to the local group
	localGroup:insert(displayClass_button)
	

	
	
	-- handle the applicationExit event to close the db
 	local function onSystemEvent( event )
   	if( event.type == "applicationExit") then
     	db:close()
   	end
 	end 
 
-- system listener for applicationExit to handle closing database
 Runtime:addEventListener ("system", onSystemEvent)
end

function scene:enterScene(event)
	storyboard.purgeScene("menu")
	localGroup.alpha = 1
	
end

function scene:exitScene(event)
     localGroup.alpha=0

end

-- "createScene" is called whenever the scene is FIRST called
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )	
	
return scene
	



