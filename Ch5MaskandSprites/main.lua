-- Project: MaskandSprites-- Description:---- Version: 1.0-- Managed with http://CoronaProjectManager.com---- Copyright 2011 Brian Burton. All Rights Reserved.---- cpmgen main.lua


local sprite = require("sprite")


local sheet1 = sprite.newSpriteSheet("greenDinoSheet.png", 128, 128)local spriteSet1 = sprite.newSpriteSet(sheet1, 2, 8)sprite.add(spriteSet1, "greenWalk", 2, 8, 2000, -1)local instance1 = sprite.newSprite(spriteSet1)instance1.x = display.contentWidth/2-128instance1.y = display.contentHeight/2instance1:prepare("greenWalk")instance1:play()

local mask = graphics.newMask("circlemask.png")instance1:setMask(mask)instance1.maskScaleX = .5instance1.maskScaleY= .5local sheetData = require("redDinoSheet")local spriteData = sheetData.getSpriteSheetData()local sheet2 = sprite.newSpriteSheetFromData("redDinoSheet.png", spriteData )local spriteSet2 = sprite.newSpriteSet(sheet2, 1, 9)sprite.add(spriteSet2, "redRoar", 1, 9, 1000, 0)local instance2 = sprite.newSprite(spriteSet2)instance2.x = display.contentWidth/2 + 128instance2.y = display.contentHeight/2instance2:prepare("redRoar")instance2:play()