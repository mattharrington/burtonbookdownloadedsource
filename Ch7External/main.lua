--
-- Project: Ch7 External
-- Description: 
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- Copyright 2011 Brian Burton. All Rights Reserved.
-- 
local external = require("external")

-- call the external function hiDad() stored in external.lua
external.hiDad()

-- cache the external function hiDad() in memory
local hi = external.hiDad

-- call the cached hiDad()
hi()

