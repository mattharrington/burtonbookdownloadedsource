-- Project: Ch16 Rotten Apples
-- Managed with http://CoronaProjectManager.com
--
-- Copyright 2011 Brian Burton. All Rights Reserved.
--local director = require("director")

display.setStatusBar(display.HiddenStatusBar)
local loqsprite = require("loq_sprite")
local lime = require("lime")
local physics = require("physics")
physics.start()
physics.setGravity(0,0)
local runnerFactory = loqsprite.newFactory("runnersheet") -- load spriteLoq sheet

-- A few variables to keep track of game elements

local runner = {} 	  -- array for tracking runners
local waypoint = {}  -- array for tracking waypoints
local runnerCount=0   -- how many runners are on the screen?local apple = {} 		-- array for tracking thrown appleslocal appleCount = 0 -- How many apples have been thrown?

local tower = {}  	   -- array for tracking towers
local towerCount=0

--[[local clubhouse = {}  -- array for tracking status of clubhouse
local tick                     -- game speed
]]

--[[   Game parameters
	runner[].speed  -- the speed of the runner (1 - normal, 2 - twice as fast)
	runner[].hp  -- base hitpoints (how many hits to disable)
	
  tower[].speed - how quickly the tower can throw again
	tower[].range - current range of tower in pixels   		-- default is 80
	tower[].damage - amount of damage
	tower.x, thrower.y 																								-- tower location	tower[].attackArea = display.newCircle(tower[].x+16, tower[].y, tower[].range)
  tower[].attackArea:setFillColor(255,255,255,15)
  tower[].attackArea.objType = "tower"               				-- id for towers  tower[].attackArea.myName   																-- green, red or rotten tower
  tower[towerCount].attackArea.isSensor=true   					-- the area around the tower acts as a sensor to register collisions
  tower[towerCount].attackArea.throwSpeed =1000      -- milliseconds between throws
  tower[towerCount].attackArea.timeTilThrow = 0        --milliseconds until can throw again
  tower[towerCount].attackArea.damage =1                 -- amount of damage  	apple[].speed  - how quickly the apple moves toward the runner	apple[].objType - apple for id purposes	apple[].myName - type of apple (green, red, rotten)	apple[].timeToLive - track apple time for deleting so the screen doesn't fill with missed shots
	
	tick - how quickly to do a new frame
	applesCollected - base number of apples to start level
	level - current level (could be used as a multiplier).	
	
	Should I use an external file to load the runner sequence?
	
	]]

local function pathfinder(event)
    print("Pathfinder was called")
	local myObj = {}
	local WPHit = {}
	if(event.object1.objType=="Runner") then
		myObj = event.object1
		WPHit = event.object2
	elseif (event.object2.objType=="Runner") then
		myObj = event.object2
		WPHit = event.object1
	else
		return  -- collision was not with a runner so it doesn't need processed
	end
print("My Name 1: "..myObj.myName..", "..myObj.objType)
print("My Name 2: "..WPHit.myName..", "..WPHit.objType)
--print("waypoint goal: "..myObj.nextWP.." WP Hit: "..WPHit.objType)

    -- Check if Runner collision is a waypoint; handle change in direction if it is.

    if(WPHit.objType == "waypoint") then
        local wpgoal = "Waypoint"..myObj.nextWP
        --Check to make sure it is the next waypoint, if not return
        if(wpgoal ~= WPHit.myName) then
	        return	-- just hit the same waypoint again, no need to process
	    elseif (myObj.nextWP+1 ~= nil) then
		    myObj.nextWP = myObj.nextWP+1
		    if (waypoint[myObj.nextWP-1].x < waypoint[myObj.nextWP].x) then
			    print("Load Runner right")
			    transition.cancel(myObj)
			    myObj:prepare(myObj.myName.."right")
			    myObj:play()
			    transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = (waypoint[myObj.nextWP-1].baseTime*myObj.speed)})
 		   elseif (waypoint[myObj.nextWP-1].x > waypoint[myObj.nextWP].x) then
			    print(myObj.myName.."left")
		        transition.cancel(myObj)
			    myObj:prepare(myObj.myName.."left")
			    myObj:play()
			    transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time =(waypoint[myObj.nextWP-1].baseTime*myObj.speed)})
            elseif (waypoint[myObj.nextWP-1].y < waypoint[myObj.nextWP].y) then
                print(myObj.myName.."down")
                transition.cancel(myObj)
                myObj:prepare(myObj.myName.."down") 
			    myObj:play()
			    transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = (waypoint[myObj.nextWP-1].baseTime*myObj.speed)})
		    elseif (waypoint[myObj.nextWP-1].y > waypoint[myObj.nextWP].y) then
			    print(myObj.myName.."up")
			    transition.cancel(myObj)
			    myObj:prepare(myObj.myName.."up")
			    myObj:play()
			    transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = (waypoint[myObj.nextWP-1].baseTime*myObj.speed)})
		    end
	    else
		     print("Reached the clubhouse!")
        end
    end
        --Check if the runner is in range of a thrower/tower    if(WPHit.objType == "tower") then      	if( WPHit.timeTilThrow <= 0) then 			-- enough time has passed that we can throw again --   		WPHit:play()										-- play throw animation 			  WPHit.timeTilThrow = WPHit.throwSpeed				print("Tower Number: "..WPHit.number)    		    apple[WPHit.number][1].alpha=1	    		apple[WPHit.number][1].active = true   			 apple[WPHit.number][1]:setLinearVelocity((myObj.x - WPHit.x),(myObj.y-WPHit.y))      	end    end    	    	-- handle apple/runner collisions			    if(WPHit.objType=="apple") then    	myObj.hp= myObj.hp-WPHit.damage    	WPHit.alpha=0    	WPHit.x, WPHit.y = tower[WPHit.tower].x, tower[WPHit.tower].y    	WPHit.active=false    	print("add to score and orchard count")    	if myObj.hp == 0 then    			print("Do star sequence")    	end    end    
end

local function addRunner ()
	 runnerCount=runnerCount+1
     local whichRunner = math.random(3)
     if(whichRunner == 1) then
     	runner[runnerCount] = runnerFactory:newSpriteGroup("green runner up")
		 runner[runnerCount].speed = 1
		 runner[runnerCount].hp = 3
		 runner[runnerCount].myName="green runner "
     elseif(whichRunner==2) then
     	runner[runnerCount] = runnerFactory:newSpriteGroup("red runner up")
		 runner[runnerCount].speed = .8
		 runner[runnerCount].hp = 5
		 runner[runnerCount].myName="red runner "   
     elseif(whichRunner==3) then
     	runner[runnerCount] = runnerFactory:newSpriteGroup("rotten runner up")
		 runner[runnerCount].speed = 1.1
		 runner[runnerCount].hp = 10
		 runner[runnerCount].myName="rotten runner "	
     end
     runner[runnerCount].x=waypoint[1].x-16
	 runner[runnerCount].y=waypoint[1].y-16
	 runner[runnerCount].nextWP = 1
	 runner[runnerCount].objType="Runner"
	 runner[runnerCount]:play()
	 physics.addBody(runner[runnerCount], {density = 10, bounce = 0, filter = {categoryBits =2, maskBits =1}})
end
local function orchard ()	apple[towerCount] = {}	for i = 1,5 do		apple[towerCount][i]=runnerFactory:newSpriteGroup("apple Green")  -- load the green apple animation    	apple[towerCount][i].myName = "green apple"		apple[towerCount][i].damage = 1    	apple[towerCount][i]:play()							-- play the animation    	apple[towerCount][i].speed = 1					-- base speed of apple    	apple[towerCount][i].objType = "apple"    	apple[towerCount][i].timeToLive = 4000 		-- if apple doesn't hit anything for 4 seconds, return to tower    	apple[towerCount][i].isBullet = true    	apple[towerCount][i].alpha = 0					-- hide apple until we need it    	apple[towerCount][i].active = false 				-- is the apple in use?    	myObj.myName.."		apple[towerCount][i].x, apple[towerCount][1].y = tower[towerCount].x, tower[towerCount].y		physics.addBody(apple[towerCount][1], {density = .01, bounce = 0, filter={categoryBits=1, maskBits=2}})	endend

-- Load and build map
local map = lime.loadMap("Lvl1.tmx")
local visual = lime.createVisual(map)
local physical = lime.buildPhysical(map)

local tilesObj= map:getObjectsWithName("WP")

	for i = 1, #tilesObj, 1 do
		waypoint[i]="Waypoint"..i
	end
	for i = 1, #tilesObj, 1 do
		for j = 1, #tilesObj, 1 do
			if tilesObj[j].type== waypoint[i] then
				waypoint[i]={}
			    waypoint[i].objType = tilesObj[j].objType				waypoint[i].x,waypoint[i].y = tilesObj[j]:getPosition()
				waypoint[i].baseTime=tilesObj[j].baseTime
			     waypoint[i].myName=tilesObj[j].myName
			     print("Waypoint "..i..": x"..waypoint[i].x.." y "..waypoint[i].y.." objType: "..waypoint[i].objType)
			end
		end	

	end
	
local addTower = function()
	towerCount = towerCount+1
	tower[towerCount]=runnerFactory:newSpriteGroup("green apple thrower")
	tower[towerCount].range= 80          -- current range of tower in pixels
	tower[towerCount].x =  96
	tower[towerCount].y = 192	
	tower[towerCount].attackArea = display.newCircle(tower[towerCount].x+16, tower[towerCount].y, tower[towerCount].range)
	tower[towerCount].attackArea:setFillColor(255,255,255,15)
	tower[towerCount].attackArea.objType = "tower"	tower[towerCount].attackArea.myName = "green tower"	tower[towerCount].attackArea.number=towerCount
	physics.addBody(tower[towerCount].attackArea, {filter={categoryBit=4, maskBit=0}})
	tower[towerCount].attackArea.isSensor=true
	tower[towerCount].attackArea.throwSpeed =1000      -- milliseconds between throws
	tower[towerCount].attackArea.timeTilThrow = 0        --milliseconds until can throw again
	tower[towerCount].attackArea.damage =1                 -- amount of damage
	tower[towerCount]:play()	orchard()   															--create 5 apples for each tower 
end	
	
addTower()


--Create Runner
addRunner()
timer.performWithDelay(2000, addRunner)
timer.performWithDelay(4000, addRunner)

--atrace(xinspect(runner[1]:getSpriteNames()))


local update = function( event )
	map:update( event )
end

Runtime:addEventListener( "enterFrame", update )
Runtime:addEventListener("collision", pathfinder)

--require('loq_profiler').createProfiler()