-- Project Ch 10 Star Explorer
-- Splash Screen with action
-- Description: Loads Splash Screen in sections as various parts of the app are loaded.


module(..., package.seeall)

-- Create a display group to handle dismissing splash screen when finished.
-- Can be local since it will only be called in splash.lua
 local splashGroup = display.newGroup()
 fullSplash = display.newImageRect("images/StarExplorerSplash.png", 320, 480) fullSplash.alpha = 0
-- Load background
function splashBackground()
	splashBg = display.newImageRect(splashGroup, "images/bg1.png",480, 480 )
	splashBg.x=display.contentWidth/2
	splashBg.y=display.contentHeight/2

	splashShip = display.newImageRect(splashGroup, "images/starfighter1.png", 74, 80)
	splashShip.x = display.contentWidth/2
	splashShip.y=display.contentHeight-80
	transition.to(splashShip, {y=display.contentHeight/2, xScale=1.5, yScale=1.5, time  = 1500})
	end

-- Move ship
function splashMoveShip()
	transition.to(splashShip,{x=50, y=display.contentHeight-50, xScale=1, yScale=1, time=1500})

	splashBigAsteroid = display.newImageRect(splashGroup, "images/asteroids1.png", 150, 129)	splashBigAsteroid.x = display.contentWidth+75	splashBigAsteroid.y = -70	transition.to(splashBigAsteroid, {x=display.contentWidth - 100, y=100, rotation = 360, time=6000})
end

-- Shoot Asteroid
function splashShoot()
	transition.to (splashShip, {y = display.contentHeight/2, rotation = 45, time = 1500})	timer.performWithDelay(2000, shoot )	timer.performWithDelay(2200, explosion)end
function shoot()	shot = display.newImageRect(splashGroup, "images/shot.png", 10, 35)	shot.x=95	shot.y=(display.contentHeight/2)-39	shot.rotation=45	 transition.to(shot, {x=display.contentWidth - 120, y = 120, time = 300})	 audio.play(fireSound)endfunction explosion()	splashLittleAsteroid1 = display.newImage(splashGroup, "images/smallasteroid.png")	splashLittleAsteroid2 = display.newImage(splashGroup, "images/smallasteroid.png")	audio.play(explosionSound)	splashLittleAsteroid1.x = display.contentWidth -120	splashLittleAsteroid1.y = 120	splashLittleAsteroid2.x = display.contentWidth-120	splashLittleAsteroid2.y = 120	transition.to(splashLittleAsteroid1, {x=-20, y = -25, rotation = 360, time = 1000})	transition.to(splashLittleAsteroid2, {x = display.contentWidth - 200, y = display.contentHeight+50, rotation = 360, time=2000})	splashBigAsteroid.alpha = 0	shot.alpha = 0	timer.performWithDelay(2000, splashScreen)end
-- Load and display final splash screen
function splashScreen()	splashGroup.alpha = 0
	fullSplash.alpha = 1	fullSplash.x = display.contentWidth/2	fullSplash.y= display.contentHeight/2	beginText=display.newText("Tap to Begin", display.contentWidth/2-50, (display.contentHeight*.75),native.systemFont, 24)end
function dismissSplashScreen()	splashBg:removeSelf()	splashShip:removeSelf()	splashBigAsteroid:removeSelf()	shot:removeSelf()	splashLittleAsteroid1:removeSelf()	splashLittleAsteroid2:removeSelf()	splashGroup:removeSelf()	fullSplash:removeSelf()	beginText:removeSelf()end