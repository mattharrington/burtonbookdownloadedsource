-- config.lua for project: PhunWithPhysics
-- Managed with http://CoronaProjectManager.com
-- Copyright 2011 Brian Burton. All Rights Reserved.
-- cpmgen config.lua
application =
{
	content =
	{
		width = 640,
		height = 960,
		scale = "letterbox",
		fps = 30,
		antialias = false,
		xalign = "center",
		yalign = "center"
	}
}