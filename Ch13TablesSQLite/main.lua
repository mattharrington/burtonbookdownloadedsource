-- Project: Ch13 SQLite Tables
-- Description:
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
---- cpmgen main.lua
display.setStatusBar( display.HiddenStatusBar ) 

local ui = require("ui")
local tableView = require("tableView")
require("sqlite3")

local stateList, cityList, zipList, backBtn, detailScreenText, selectState, selectCity
local data={}
local cityData ={}
local zipData={}
local zipScreen = display.newGroup()
local screenOffsetW, screenOffsetH = display.contentWidth -  display.viewableContentWidth, display.contentHeight - display.viewableContentHeight

--Setup the nav bar 
local navBar = ui.newButton{
	default = "images/navBar.png",
	onRelease = scrollToTop
}
navBar.x = display.contentWidth*.5
navBar.y = math.floor(display.screenOriginY + navBar.height*0.5)

local navStateHeader = display.newText("Select a State", 0, 0, native.systemFontBold, 16)
navStateHeader:setTextColor(255, 255, 255)
navStateHeader.x = display.contentWidth*.5
navStateHeader.y = navBar.y

local navCityHeader = display.newText("Select a City", 0, 0, native.systemFontBold, 16)
navCityHeader:setTextColor(255, 255, 255)
navCityHeader.x = display.contentWidth*.5
navCityHeader.y = navBar.y
navCityHeader.alpha = 0

local navZipHeader = display.newText("Zip codes for selected city ", 0, 0, native.systemFontBold, 16)
navZipHeader:setTextColor(255, 255, 255)
navZipHeader.x = display.contentWidth/2+20
navZipHeader.y = navBar.y
navZipHeader.alpha=0

-- Does the database exist in the documents directory (allows updating and persistance)
local path = system.pathForFile("zip.sqlite", system.DocumentsDirectory )
file = io.open( path, "r" )
   if( file == nil )then           
   	-- Doesn't Already Exist, So Copy it In From Resource Directory                          
   	pathSource     = system.pathForFile( "zip.sqlite", system.ResourceDirectory )  
   	fileSource = io.open( pathSource, "r" ) 
   	contentsSource = fileSource:read( "*a" )                                  
		--Write Destination File in Documents Directory                                  
		pathDest = system.pathForFile( "zip.sqlite", system.DocumentsDirectory )                 
		fileDest = io.open( pathDest, "w" )                 
		fileDest:write( contentsSource )                 
		 -- Done                      
		io.close( fileSource )        
		io.close( fileDest )         
   end   
-- One Way or Another The Database File Exists Now -- So Open Database Connection         
db = sqlite3.open( path )


-- handle the applicationExit event to close the db
local function onSystemEvent( event )
	if( event.type == "applicationExit") then
		db:close()
		stateList:cleanUp()
		cityList:cleanUp()
		zipList:cleanUp()
	end
end


function selectZipButtonRelease(event)
    --setup a destination for the detail item
    local detailScreen = display.newGroup()

    local detailBg = display.newRect(0,0,display.contentWidth,display.contentHeight-display.screenOriginY)
    detailBg:setFillColor(255,255,255)
    detailScreen:insert(detailBg)
    local self = event.target
    local tempText = zipData[self.id].." is the zip code for "..selectCity..", "..selectState
    detailScreenText = display.newText(tempText, 0, 0, native.systemFontBold, 14)
    detailScreenText:setTextColor(0, 0, 0)
    detailScreen:insert(detailScreenText)
    detailScreenText.x = math.floor(display.contentWidth/2)
    detailScreenText.y = 10	
    detailScreen.x = display.contentWidth
    
    transition.to(zipScreen, {time=400, x=display.contentWidth*-1, transition=easing.outExpo })
	transition.to(detailScreen, {time=400, x=0, transition=easing.outExpo })

end


-- after the State and City are selected, show available zip codes
function selectCityButtonRelease(event)
	local self = event.target
	selectCity = cityData[self.id]
	local count = 0
	local sql = "SELECT zip FROM zipcode WHERE state = '"..selectState.."' AND city = '"..selectCity.."'"
	print(sql)
	for row in db:nrows(sql) do
		count = count +1
		zipData[count]=row.zip
	end	
	zipScreen.alpha = 1

    local detailBg = display.newRect(0,0,display.contentWidth,display.contentHeight-display.screenOriginY)
    detailBg:setFillColor(255,255,255)
    zipScreen:insert(detailBg)
	
 zipList = tableView.newList{
        data=zipData,
        default="images/listItemBg.png",
        over="images/listItemBg_over.png",
        onRelease=selectZipButtonRelease,
        top=60,
        bottom=1,
        callback=
              function(item) 
                        local t = display.newText(zipScreen, item, 20, 0, native.systemFontBold, textSize)
                        t:setTextColor(0,0,0)
                        t.x = math.floor(t.width/2) + 20
                        t.y = 46 
                        return t
                end
} 
	transition.to(CityList, {time=400, x=display.contentWidth*-1, transition=easing.outExpo })
	transition.to(zipScreen, {time=400, x=0, transition=easing.outExpo })
	transition.to(zipBackBtn, {time=400, x=math.floor(backBtn.width/2) + screenOffsetW*.5 + 6, transition=easing.outExpo })
	transition.to(zipBackBtn, {time=400, alpha=1 })
	navBar:toFront()
	zipBackBtn:toFront()
	navZipHeader:toFront()
	transition.to(navZipHeader, {time=400,alpha = 1})
	transition.to(navCityHeader, {time=400, alpha = 0})
	
	delta, velocity = 0, 0
end


function backBtnRelease( event )  -- reload the state table view
	print("back button released")
	transition.to(stateList, {time=400, x=0, transition=easing.outExpo })
	transition.to(cityList, {time=400, x=display.contentWidth, transition=easing.outExpo })
	transition.to(backBtn, {time=400, x=math.floor(backBtn.width/2)+backBtn.width, transition=easing.outExpo })
	transition.to(backBtn, {time=400, alpha=0 })
	cityList:cleanUp()
	delta, velocity = 0, 0
end

--setup function to execute on touch of the state list/table view items
function selectStateButtonRelease( event )
	local self = event.target
	selectState = data[self.id]
	print(selectState)
	-- pull city data from database for selected state
	local count = 0
	local sql = "SELECT DISTINCT city FROM zipcode WHERE state = '".. selectState.."'"
	for row in db:nrows(sql) do
		count = count +1
		cityData[count]=row.city
		--print(cityData[count])
	end
	-- list city thru tableview
	cityList = tableView.newList{
        data=cityData,
        default="images/listItemBg.png",
        over="images/listItemBg_over.png",
        onRelease=selectCityButtonRelease,
        top=60,
        bottom=1,
        callback=function(item) 
                        local t = display.newText(item, 0, 0, native.systemFontBold, textSize)
                        t:setTextColor(255, 255, 255)
                        t.x = math.floor(t.width/2) + 20
                        t.y = 46 
                        return t
                end
}		
	-- handle screen transitions (remove statelist, show citylist, add back button)
	transition.to(stateList, {time=400, x=display.contentWidth*-1, transition=easing.outExpo })
	transition.to(cityList, {time=400, x=0, transition=easing.outExpo })
	transition.to(backBtn, {time=400, x=math.floor(backBtn.width/2) + screenOffsetW*.5 + 6, transition=easing.outExpo })
	transition.to(backBtn, {time=400, alpha=1 })
	transition.to(navStateHeader, {time=400, alpha=0})
	transition.to(navCityHeader, {time=400, alpha = 1})
	
	delta, velocity = 0, 0
end

local getState = function()
       -- load the state array to be displayed in the table view
       -- set each row of the array as an array, then load state name and abbreviation
       local count =0
       local sql = "SELECT DISTINCT state FROM zipcode"  
       for row in db:nrows(sql) do
	       count = count +1
	       data[count] = row.state
	       --print(data[count])	
       end
       stateList = tableView.newList{
        data=data,
        default="images/listItemBg.png",
        over="images/listItemBg_over.png",
        onRelease=selectStateButtonRelease,
        top=60,
        bottom=1,
        callback=function(item) 
                        local t = display.newText(item, 0, 0, native.systemFontBold, textSize)
                        t:setTextColor(255, 255, 255)
                        t.x = math.floor(t.width/2) + 20
                        t.y = 46 
                        return t
                end
}
stateList:addScrollBar()
end

function cityBackBtnRelease( event )  -- reload the state table view
	print("city back button released")
	transition.to(stateList, {time=400, x=0, transition=easing.outExpo })
	transition.to(cityList, {time=400, x=display.contentWidth, transition=easing.outExpo })
	transition.to(backBtn, {time=400, x=math.floor(backBtn.width/2)+backBtn.width, transition=easing.outExpo })
	transition.to(backBtn, {time=400, alpha=0 })
	transition.to(navStateHeader, {time = 400, alpha = 1})
	navStateHeader:toFront()
	transition.to(navCityHeader, {time=400, alpha=0})
	cityList:cleanUp()
	delta, velocity = 0, 0
end

function zipBackBtnRelease( event )  -- reload the City table view
	print("zip back button released")
	zipScreen.alpha=0
	transition.to(cityList, {time=400, x=0, transition=easing.outExpo })
	cityList:toFront()
	transition.to(zipScreen, {time=400, x=display.contentWidth, transition=easing.outExpo })
	transition.to(zipBackBtn, {time=400, x=math.floor(zipBackBtn.width/2)+zipBackBtn.width, transition=easing.outExpo })
	transition.to(zipBackBtn, {time=400, alpha=0 })
	transition.to(backBtn, {time=400, alpha = 1})
	backBtn:toFront()
	transition.to(navCityHeader, {time=400, alpha=1})
	navCityHeader:toFront()
	transition.to(navZipHeader,{time=400, alpha=0})
	zipList:cleanUp()
	delta, velocity = 0, 0
end


--Setup the city view back button
backBtn = ui.newButton{ 
	default = "images/backButton.png", 
	over = "images/backButton_over.png", 
	onRelease = cityBackBtnRelease
}
backBtn.x = math.floor(backBtn.width/2) + backBtn.width + screenOffsetW
backBtn.y = navBar.y 
backBtn.alpha = 0

--Setup the zip view back button
zipBackBtn = ui.newButton{ 
	default = "images/backButton.png", 
	over = "images/backButton_over.png", 
	onRelease = zipBackBtnRelease
}
zipBackBtn.x = math.floor(zipBackBtn.width/2) + zipBackBtn.width + screenOffsetW
zipBackBtn.y = navBar.y 
zipBackBtn.alpha = 0


getState()

-- system listener for applicationExit
Runtime:addEventListener ("system", onSystemEvent)