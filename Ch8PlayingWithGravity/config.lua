-- config.lua for project: Ch8PlayingWithGravity
-- Managed with http://CoronaProjectManager.com
-- Copyright 2011 Brian Burton. All Rights Reserved.
-- cpmgen config.lua
application =
{
	content =
	{
		width = 768,
		height = 1024,
		scale = "letterbox",
		fps = 30,
		antialias = false,
		xalign = "center",
		yalign = "center"
	}
}