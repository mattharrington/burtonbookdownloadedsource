-- Project: Using Force
-- Description: Demonstrates applying for using various properties and methods
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--

---- cpmgen main.lua

----------
-- Intialize external libraries and start physics
----------
local ui = require("ui")
local physics = require("physics")
physics.start()
physics.setGravity(0,0)
---------
-- load a box to use for a body 
---------
local box = display.newImage("button6.png")
box.x = display.contentWidth/2
box.y = display.contentHeight/2-100
physics.addBody(box, {density = 1, friction =0.3, bounce = 0.2})


-- applyForce demonstration, centered  
local boxApplyForce = function (event)
	--return box to its starting location and reset dampening
	box.x = display.contentWidth/2-100
    box.y = display.contentHeight/2-100
    box.linearDamping = 0
    box.angularDamping = 0
    --apply force to move up and to the right
    box:applyForce(20, -20, box.x, box.y)
    
end

-- applyForce demonstration, off-center
local boxApplyForceOffCenter = function (event)
	--return box to its starting location and reset dampening
	box.x = display.contentWidth/2-100
    box.y = display.contentHeight/2-100
    box.linearDamping = 0
    box.angularDamping = 0
    -- apply force to move up and to the right, off center
    box:applyForce(20,-20, box.x-20, box.y)
    
end

-- applyTorque demonstration 
local boxApplyTorque = function (event)
	--return box to its starting location and reset dampening
	box.x = display.contentWidth/2-100
    box.y = display.contentHeight/2-100
    box.linearDamping = 0
    box.angularDamping = 0
    -- apply torque
    box:applyTorque(50)
end	

-- applyLinearImpulse
local boxApplyLinearImpulse = function (event)
	--return box to its starting location and reset dampening
	box.x = display.contentWidth/2-100
    box.y = display.contentHeight/2-100
    box.linearDamping = 0
    box.angularDamping = 0
    --apply a single impulse up and to the right
    box:applyLinearImpulse(20, -20, box.x, box.y)
end

-- applyAngularImpulse
local boxApplyAngularImpulse = function (event)
	--return box to its starting location and reset dampening
	box.x = display.contentWidth/2-100
    box.y = display.contentHeight/2-100
    box.linearDamping = 0
    box.angularDamping = 0
    --apply a single angular impulse
    box:applyAngularImpulse(50)
end


local boxlinearDamping =  function (event)
	box.linearDamping = 100
end

local boxangularDamping = function (event)
	box.angularDamping = 100
end

---------
-- Create Buttons
---------
local  applyForceButton = ui.newButton{
	default="button1.png",
	onPress=boxApplyForce,
	text= "Force, Centered",
	size = 18,	emboss = true}
	applyForceButton.x = 120
	applyForceButton.y=display.contentHeight - 400
	
local  applyForceButtonOffCenter = ui.newButton{
	default="button1.png",
	onPress=boxApplyForceOffCenter,
	text= "Force, off Center",
	size = 18,	emboss = true}
	applyForceButtonOffCenter.x = 350
	applyForceButtonOffCenter.y=display.contentHeight - 400

local  applyTorqueButton = ui.newButton{
	default="button1.png",
	onPress=boxApplyTorque,
	text= "Torque",
	size = 19,	emboss = true}
	applyTorqueButton.x = display.contentWidth/2
	applyTorqueButton.y=display.contentHeight - 300
	
	local  applyImpulseButton = ui.newButton{
	default="button1.png",
	onPress=boxApplyLinearImpulse,
	text= "Linear Impulse",
	size = 18,	emboss = true}
	applyImpulseButton.x = 120
	applyImpulseButton.y=display.contentHeight - 200

local  applyAngularImpulseButton = ui.newButton{
	default="button1.png",
	onPress=boxApplyAngularImpulse,
	text= "Angular Impulse",
	size = 18,	emboss = true }
	applyAngularImpulseButton.x = 350
	applyAngularImpulseButton.y=display.contentHeight - 200	

local  linearDampingButton = ui.newButton{
	default="button1.png",
	onPress=boxlinearDamping,
	text= "Linear Damping",
	size = 18,	emboss = true}
	linearDampingButton.x = 120
	linearDampingButton.y=display.contentHeight -100
	
	local  angularDampingButton = ui.newButton{
	default="button1.png",
	onPress=boxangularDamping,
	text= "Angular Damping",
	size = 18,	emboss = true}
	angularDampingButton.x = 350
	angularDampingButton.y=display.contentHeight -100