
--====================================================================--
-- SCENE: Menu
--====================================================================--

--[[

******************
 - INFORMATION
******************

  - Menu.

 Show buttons for selecting the display class screen or add student screen
  

  
 --]]
	
	local widget = require ( "widget" )
	local storyboard = require("storyboard")
	local scene = storyboard.newScene()
	local localGroup = display.newGroup()
	
	



--Called if the scene hasn't been previously seen
function scene:createScene( event )
		local displayClass_function = function ( event )
			storyboard.gotoScene( "displayClass", "fade", 400 )
	
	end



	local  displayClass_button = widget.newButton{
					default = "menuButton.png",
					over = "menuButton.png",
				    label = " Class List",
				    size = 24,
				    emboss=true,
					onRelease = displayClass_function,
					id = "displayClass"
	}

	
		local addStudent_function = function ( event )
			storyboard.gotoScene( "addStudent", "slideRight", 400 )
		end
		
		local addStudent_button = widget.newButton{
					default = "menuButton.png",
					over = "menuButton.png",
					label = "Add a Student",
				    size = 24,
				    emboss=true,
					onRelease = addStudent_function,
					id = "addStudent"
	}
	
	addStudent_button.x = display.contentWidth/2
	addStudent_button.y = display.contentHeight/2 - 200
	displayClass_button.x = display.contentWidth/2
	displayClass_button.y=display.contentHeight/2 + 200
	
	
	localGroup:insert(displayClass_button)
	localGroup:insert(addStudent_button)
end


function scene:enterScene(event)
	localGroup.alpha = 1
	storyboard.purgeAll()
end

function scene:exitScene(event)
localGroup.alpha = 0

end

-- "createScene" is called whenever the scene is FIRST called
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )
	return scene