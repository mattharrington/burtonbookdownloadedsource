-- Created by: Dr. Brian Burton, Burton's Media Group.
-- 
-- Project 4.0 Calculator

-- load buttons and place on the display
local bkgd = display.newImage("bkgd.png",0,0)
local width = display.contentWidth/2
local height = display.contentHeight/2 - 100
local addButton = display.newImage("add.png", width -145, height)
local subtractButton = display.newImage("subtract.png", width -60, height)
local multiplyButton = display.newImage("multiply.png", width +15, height)
local divideButton = display.newImage("divide.png", width + 85, height)
local equalButton = display.newImage("equals.png", width, height + 150)

-- Textbox for first number
--local firstNumber = 10
--local firstNumber = display.newText(firstNumber, width + 100, height - 75, native.systemFont, 36)
local firstNumber = native.newTextField(width - 100, height - 75, 220, 55)
firstNumber.inputType="number"

-- Textbox for second number
--local secondNumber = 5
--local secondNumber = display.newText(secondNumber, width + 100, height +75, native.systemFont, 36)
local secondNumber = native.newTextField(width - 100, height + 75, 220, 55)
secondNumber.inputType="number"

local operator   -- variable to tell us which operator was selected
local result     -- variable to hold the result
local operandSelected = "False"
local resultText
local warningText

local function addButtonTap(event)
	operator = "+"
	operandSelected = "True"

end
local function subtractButtonTap(event)
	operator = "-"
	operandSelected = "True"
end
local function multiplyButtonTap(event)
	operator = "*"
	operandSelected = "True"
end
local function divideButtonTap(event)
	operator = "/"
	operandSelected = "True"
end


local function equalButtonTap(event) 	
   
  if operandSelected == "True" then
	if operator == "+" then
		result = tonumber(firstNumber.text) + tonumber(secondNumber.text)
	elseif operator == "-" then
		result = tonumber(firstNumber.text) - tonumber(secondNumber.text)
	elseif operator == "*" then
		result = tonumber(firstNumber.text) * tonumber(secondNumber.text)
	elseif operator == "/" then
		result = tonumber(firstNumber.text) / tonumber(secondNumber.text)
	end
	local clearRect = display.newRect(0, 350, display.contentWidth, 60)
clearRect:setFillColor(0,0,0)
	local resultText = display.newText(result, width, height+200, native.systemFont, 36)
	resultText:setTextColor(255,255,255)

  	operandSelected = "False"
  else
  	local warningText = display.newText("Select operation first", width-150, 100, native.systemFont, 36)
  	warningText:setTextColor(255,255,255)
  end	
end	

local listener = function (event)
	native.setKeyboardFocus(nil)
end

addButton:addEventListener("tap", addButtonTap)
subtractButton:addEventListener("tap", subtractButtonTap)
multiplyButton:addEventListener("tap", multiplyButtonTap)
divideButton:addEventListener("tap", divideButtonTap)
equalButton:addEventListener("tap", equalButtonTap)
bkgd:addEventListener("tap", listener)