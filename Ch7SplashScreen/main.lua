-- Project: Ch7SplashSreen
-- Description:
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- Copyright 2011 Brian Burton. All Rights Reserved.
---- cpmgen main.lua

local external = require("external")

local function splash()    -- Create a group to make dismissing the splash screen easy   splashGroup = display.newGroup()  	-- Create a background with from a vector rectangle.  Must be a global variable since it is called outside of the function	bg = display.newRect(splashGroup, 0, 0, 320, 480)	bg:setFillColor( 10, 10, 200)				-- Add text object of app title	local  splashText = display.newText(splashGroup, "Hi\n Dad!", 100, 150,native.systemFont, 40 )	splashText.rotation=-30		-- Tell the user how to proceed	local proceedText = display.newText(splashGroup, "Tap To Give A Shout Out", display.contentWidth/2-100, display.contentHeight-100)	endlocal function bgButton(event)	-- handle dismissing the splash screen when it is tapped by fading out the splashGroup	transition.to(splashGroup, {alpha = 0, time = 3000})		-- pass control to the main function	main()endfunction main()	   -- remove splashGroup from memory	   splashGroup:removeSelf()       -- cache the external function hiDad() in memory
       local hi = external.hiDad
         -- call the cached hiDad()
         hi()
end-- now call the splash screen and add the event listener for the background
splash()bg:addEventListener("tap", bgButton)



