-- Project: Ch16 Rotten Apples
-- Managed with http://CoronaProjectManager.com
--

display.setStatusBar(display.HiddenStatusBar)
local ui = require("ui")
local loqsprite = require("loq_sprite")
local lime = require("lime")
local physics = require("physics")
physics.start()
physics.setGravity(0,0)
local runnerFactory = loqsprite.newFactory("runnersheet") -- load spriteLoq sheet

-- A few variables to keep track of game elements
local hit = audio.loadSound("hit 1.wav")
local runner = {} 	  -- array for tracking runners
local waypoint = {}  -- array for tracking waypoints
local runnerCount=0   -- how many runners are on the screen?
local apple = {} 		-- array for tracking thrown apples
local appleCount = 0 -- How many apples have been thrown?
local sound = true      -- Should sound effects play?
local paused = false   -- Is the game paused?
local textScore
local textDamage
local textWave
local textApples
local sndImage 
local pauseImage
local splash
local newGameButton
local loadGameButton
local tower = {}  	   -- array for tracking towers
local towerTiles = {}    -- array for tracking map tiles and where towers are set.
local towerCount=0
local level = 1
local levelStarted = false
local tick = 400 		-- game loop speed   
local map 				-- stores the tiled map
local visual 				-- stores viewable tiled map
local physical 			-- holds the physical from tiled
local tilesObj			 --holds the tiled objects from tiled
local score = 0		-- Game score
local applesCollected = 150  -- apples gathered from knocking out attackers
local damage = 0          -- damage done to clubhouse
local clubhouse = {}
local greentower
local redtower
local rottentower

local wave ={}        -- array for waves -- 1 = green, 2 = red, 3 = rotten, 0 = random
local waveCount = 10
	wave[1] = {1,1,1,1,1,1,1,1,1,1}
	wave[2] = {1,1,1,1,2,1,1,1,1,2}
	wave[3] = {1,2,2,2,2,3,2,1,1,2}
	wave[4] = {2,2,3,1,2,2,3,2,3,3}
	wave[5] = {3,2,1,3,2,1,3,2,1,3}
	wave[6] = {3,2,2,1,3,2,2,1,0,0}
	wave[7] = {0,0,0,0,0,0,0,0,0,0}
	wave[8] = {3,0,3,0,3,0,3,3,2,0,3,1,2,2,2}
	wave[9]= {3,0,0,3,0,3,0,3,3,3,0,0,0,0,3,3,0,0,3,0}
	wave[10]={3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3}

local function clubhouseDamage()
	  print("Reached the clubhouse!")
	  damage = damage + 1
	  if(damage < 3) then
	  	clubhouse:prepare("club house75")
	  elseif(damage < 6) then
	  	clubhouse:prepare("club house50")
	  elseif(damage < 8) then
	  	clubhouse:prepare("club house25")
	  elseif(damage >= 10) then
	  	clubhouse:prepare("club house00")
	  	print("Game Over!")
	  end
	  	
end


local function pathfinder(event)
   -- print("Pathfinder was called")
	local myObj = {}
	local WPHit = {}

	if(event.object1.objType=="Runner") then
		myObj = event.object1
		WPHit = event.object2
	elseif (event.object2.objType=="Runner") then
		myObj = event.object2
		WPHit = event.object1
	else
		return  -- collision was not with a runner so it doesn't need processed
	end
--print("My Name 1: "..myObj.myName..", "..myObj.objType)
--print("My Name 2: "..WPHit.myName..", "..WPHit.objType)
--print("waypoint goal: "..myObj.nextWP.." WP Hit: "..WPHit.objType)

    -- Check if Runner collision is a waypoint; handle change in direction if it is.

    if(WPHit.objType == "waypoint") then
    	if(WPHit.myName == "Finish" ) then
			myObj:removeSelf()
			myObj.myName = nil
			clubhouseDamage()
        else
          local wpgoal = "Waypoint"..myObj.nextWP
        --Check to make sure it is the next waypoint, if not return
        if(wpgoal ~= WPHit.myName) then
	        return	-- just hit the same waypoint again, no need to process
	    elseif (myObj.nextWP+1 ~= nil) then
		    myObj.nextWP = myObj.nextWP+1
			myObj.currentTransition = transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = (waypoint[myObj.nextWP-1].baseTime*myObj.speed)})
			myObj.baseTime=waypoint[myObj.nextWP-1].baseTime 		--to track how far along the path the runner is incase of pause
		    if (waypoint[myObj.nextWP-1].x < waypoint[myObj.nextWP].x) then
--			        print(myObj.myName.."right")
			    myObj:prepare(myObj.myName.."right")
			    myObj:play()
 		   elseif (waypoint[myObj.nextWP-1].x > waypoint[myObj.nextWP].x) then
--			    print(myObj.myName.."left")
			    myObj:prepare(myObj.myName.."left")

			    myObj:play()
            elseif (waypoint[myObj.nextWP-1].y < waypoint[myObj.nextWP].y) then
  --              print(myObj.myName.."down")
                myObj:prepare(myObj.myName.."down")
			    myObj:play()
		    elseif (waypoint[myObj.nextWP-1].y > waypoint[myObj.nextWP].y) then
--			         print(myObj.myName.."up")
			    myObj:prepare(myObj.myName.."up")
			    myObj:play()
		    end
        end
    	end
        return   -- no need to run the rest of the function
    end
    
    --Check if the runner is in range of a thrower/tower
    if(WPHit.objType == "tower") then  
    	if( WPHit.timeTilThrow <= 0) then 			-- enough time has passed that we can throw again
 --   			print("Apple launched!")
				WPHit.timeTilThrow = WPHit.throwSpeed
    		    apple[WPHit.number][tower[WPHit.number].appleCount].alpha=1
	    		apple[WPHit.number][tower[WPHit.number].appleCount].active = true
--	    		print("values for linear velocity "..(myObj.x - WPHit.x)..", "..(myObj.y-WPHit.y))
   			 apple[WPHit.number][tower[WPHit.number].appleCount]:setLinearVelocity((myObj.x - WPHit.x),(myObj.y-WPHit.y))
   	 		WPHit.needApple = true  
    	end
    	return  -- no need to run the rest of the function
    end
    	
    	-- handle apple/runner collisions			
    if(WPHit.objType=="apple") then
    	myObj.hp= myObj.hp-WPHit.damage
--		print(WPHit.myName)
		if(WPHit~=nil) then
			WPHit:removeSelf()
			WPHit=nil
		end
		if sound then
					audio.play(hit)
		end
--    	print("add to score and orchard count")
    	if (myObj.hp <= 0 and myObj.myName ~= nil) then
--    			print("Do star sequence")
    		    transition.cancel(myObj.currentTransition)
			    myObj:prepare(myObj.myName.."stars")
			    myObj:play()
		    	transition.to(myObj, {time = 2000, alpha = 0})
		    	applesCollected = applesCollected + myObj.appleValue
		    	score = score + myObj.score
		    	myObj.myName = "down"
		    	myObj.objType="down"
    	end
    end
  
end



local function addRunner (whichRunner)
	if levelStarted == false then
		levelStarted = true
	end
	if(whichRunner==0) then
		whichRunner= math.random(1,3)
	end
	 runnerCount=runnerCount+1
     if(whichRunner == 1) then
     	runner[runnerCount] = runnerFactory:newSpriteGroup("green runner up")
		 runner[runnerCount].speed = 1
		 runner[runnerCount].hp = 3
		 runner[runnerCount].myName="green runner "
		 runner[runnerCount].appleValue = 1
		 runner[runnerCount].score = 3
     elseif(whichRunner==2) then
     	runner[runnerCount] = runnerFactory:newSpriteGroup("red runner up")
		 runner[runnerCount].speed = .8
		 runner[runnerCount].hp = 5
		 runner[runnerCount].myName="red runner "  
		 runner[runnerCount].appleValue = 3
		 runner[runnerCount].score = 5
     elseif(whichRunner==3) then
     	runner[runnerCount] = runnerFactory:newSpriteGroup("rotten runner up")
		 runner[runnerCount].speed = 1.1
		 runner[runnerCount].hp = 10
		 runner[runnerCount].myName="rotten runner "	
		 runner[runnerCount].appleValue = 5
		 runner[runnerCount].score = 10
     end
     runner[runnerCount].x=waypoint[1].x-16
	 runner[runnerCount].y=waypoint[1].y-16
	 runner[runnerCount].nextWP = 1
	 runner[runnerCount].objType="Runner"
	 runner[runnerCount].baseTime=0
	 runner[runnerCount].currentTransition = nil   -- placeholder so that transition can be canceled
	 runner[runnerCount]:play()
	 physics.addBody(runner[runnerCount], {density = 10, bounce = 0, filter = {categoryBits =2, maskBits =1}})
end


local function orchard(towerCounter)
	    local i= tower[towerCounter].appleCount + 1
		tower[towerCounter].attackArea.needApple = false
		tower[towerCounter].appleCount =i
		apple[towerCounter] = {}
		if(tower[towerCounter].attackArea.myName =="green tower") then
	    			apple[towerCounter][i]=runnerFactory:newSpriteGroup("apple Green")  -- load the green apple animation
			    	apple[towerCounter][i].myName = "green apple"

		elseif(tower[towerCounter].attackArea.myName =="red tower") then
					 apple[towerCounter][i]=runnerFactory:newSpriteGroup("apple Red")  -- load the red apple animation
					 apple[towerCounter][i].myName = "red apple"

		elseif(tower[towerCounter].attackArea.myName =="rotten tower") then
			    	apple[towerCounter][i]=runnerFactory:newSpriteGroup("apple Rotten")  -- load the rotten apple animation
			    	apple[towerCounter][i].myName = "rotten apple"
		end
    	apple[towerCounter][i]:play()							-- play the animation
		apple[towerCounter][i].damage = tower[towerCounter].attackArea.damage
    	apple[towerCounter][i].objType = "apple"
    	apple[towerCounter][i].timeToLive = 3500 		-- if apple doesn't hit anything for 3.5 seconds, remove
    	apple[towerCounter][i].isBullet = true
    	apple[towerCounter][i].alpha = 0					-- hide apple until we need it
    	apple[towerCounter][i].active = false 				-- is the apple in use?
		apple[towerCounter][i].x, apple[towerCounter][i].y = tower[towerCounter].x, tower[towerCounter].y
		physics.addBody(apple[towerCounter][i], {density = .01, bounce = 0, filter={categoryBits=1, maskBits=2}})
	
end


	
local addTower = function(towerType, towerx, towery)
	towerCount = towerCount+1

	if(towerType =="green") then
		tower[towerCount]=runnerFactory:newSpriteGroup("green apple thrower")
		tower[towerCount].range= 80          -- base range of tower in pixels
		tower[towerCount].x = towerx
		tower[towerCount].y = towery		tower[towerCount].number=1
		tower[towerCount].attackArea = display.newCircle(tower[towerCount].x, tower[towerCount].y, tower[towerCount].range)
		tower[towerCount].attackArea.myName = "green tower"
		tower[towerCount].attackArea.damage =1                 -- amount of damage
	elseif(towerType=="red") then
		tower[towerCount]=runnerFactory:newSpriteGroup("red apple thrower")
		tower[towerCount].range= 80          -- base range of tower in pixels
		tower[towerCount].x = towerx
		tower[towerCount].y = towery
		tower[towerCount].number=2
		tower[towerCount].attackArea = display.newCircle(tower[towerCount].x, tower[towerCount].y, tower[towerCount].range)
		tower[towerCount].attackArea.myName = "red tower"
		tower[towerCount].attackArea.damage =2                 -- amount of damage
	elseif(towerType=="rotten") then
		tower[towerCount]=runnerFactory:newSpriteGroup("rotten apple thrower")
		tower[towerCount].range= 80          -- base range of tower in pixels
		tower[towerCount].x = towerx
		tower[towerCount].y = towery
		tower[towerCount].number=3
		tower[towerCount].attackArea = display.newCircle(tower[towerCount].x, tower[towerCount].y, tower[towerCount].range)
		tower[towerCount].attackArea.myName = "rotten tower"
		tower[towerCount].attackArea.damage =3                 -- amount of damage
	end

	tower[towerCount].appleCount=0
	tower[towerCount].attackArea:setFillColor(255,255,255,15)
	tower[towerCount].attackArea.objType = "tower"

	tower[towerCount].attackArea.number=towerCount
	physics.addBody(tower[towerCount].attackArea, {filter={categoryBit=4, maskBit=0}})
	tower[towerCount].attackArea.isSensor=true
	tower[towerCount].attackArea.throwSpeed =1000      -- milliseconds between throws
	tower[towerCount].attackArea.timeTilThrow = 0        --milliseconds until can throw again

	tower[towerCount].attackArea.needApple = true

	orchard(towerCount)   															--create first apple for tower 

end	
	

local function startDrag( event )
	if paused then
		return
	end
	local t = event.target

	local phase = event.phase
	if "began" == phase then
		display.getCurrentStage():setFocus(t)
		t.isFocus = true
		
		--Store inital position
		t.x0 = event.x - t.x
		t.y0 = event.y - t.y
		
	elseif t.isFocus then
		if "moved" == phase then
			t.x = event.x - t.x0
			t.y = event.y - t.y0
		elseif "ended" == phase or "cancelled" == phase then
			t.isFocus = false
			display.getCurrentStage():setFocus(nil)
			if(t.cost <= applesCollected) then
				applesCollected=applesCollected - t.cost
				print("cost "..t.cost..", "..applesCollected)
			else
				print("Not enough apples to purchase this thrower!")
				print("cost "..t.cost..", "..applesCollected)
				t.x=t.initialX
				t.y=t.initialY
				return
			end
			local dropLocation = {x = t.x, y = t.y}
			if(map:getTileAt(dropLocation) ~= nil) then     -- was the drop made on the map?
			    local testTile = map:getTileAt(dropLocation)
				local found = false
				local i = 0
				while (not found) do
					i = i + 1
					if(towerTiles[i].row == testTile.row and towerTiles[i].column == testTile.column and towerTiles[i].tower == 0) then
						-- good location
						towerTiles[i].tower = t.myName
						addTower(t.myName, t.x, t.y)
						t.x=t.initialX
		    			t.y=t.initialY
		    			found = true
					end
					if (i == #towerTiles) then
	    				-- location wasn't found or was bad/inuse
		    			print("YOU CAN'T PUT THAT THERE! Stay out of the Road!")  
						t.x=t.initialX  -- return the drag and drop tower to the side
		    			t.y=t.initialY
		    			found = true -- but not really, just need to break out of the loop
					end
			end
			else
				-- location wasn't found or was bad/inuse
		    	print("YOU CAN'T PUT THAT THERE!  Off Map")  
				t.x=t.initialX  -- return the drag and drop tower to the side
		    	t.y=t.initialY
		end

		end
	end

	return true
end



-- Display lives and score
local function initializeStatus()
   textApples = display.newText("Apples: "..applesCollected, display.contentWidth - 120, 30, nil, 12)
   textScore = display.newText("Score: "..score, display.contentWidth - 120, 10, nil, 12)
   textDamage = display.newText("Damage: "..damage, display.contentWidth -120, 50, nil,12)
   textWave = display.newText("Wave "..waveCount.." of 10", display.contentWidth -120, 70,nil, 12)
   textApples:setTextColor(255,255,255)
   textScore:setTextColor(255,255,255)
   textDamage:setTextColor(255,255,255)
   textWave:setTextColor(255,255,255)
end


local function updateScore()
	    textApples.text = "Apples: "..applesCollected
    	textScore.text = "Score:  "..score
		textDamage.text="Damage: "..damage
		textWave.text="Wave "..waveCount.." of 10"
end


local function initializeTowers()
		greentower= runnerFactory:newSpriteGroup("green apple thrower")
		greentower.myName="green"
		greentower.x=display.contentWidth-100
		greentower.initialX=display.contentWidth-100
		greentower.y=150
		greentower.initialY=150
		greentower.objType= "prototower"
		greentower:addEventListener( "touch", startDrag )
		greentower.cost = 50
		redtower= runnerFactory:newSpriteGroup("red apple thrower")
		redtower.myName="red"
		redtower.x=display.contentWidth-100
		redtower.initialX=display.contentWidth-100
		redtower.y=250
		redtower.initialY=250
		redtower.objType= "prototower"
		redtower:addEventListener( "touch", startDrag )
		redtower.cost=75
		rottentower= runnerFactory:newSpriteGroup("rotten apple thrower")
		rottentower.myName="rotten"
		rottentower.x=display.contentWidth-100
		rottentower.initialX=display.contentWidth-100
		rottentower.y=350
		rottentower.initialY=350
		rottentower.objType= "prototower"
		rottentower:addEventListener( "touch", startDrag )
		rottentower.cost=100
end


local function startWave(waveNumber)
		local AR 
		local timerdelay = 0
		for i = 1, #wave[waveNumber] do
		AR = function() return addRunner(wave[waveNumber][i]) end   -- must use a closure when using perform with delay and want to send data
			timerdelay = timerdelay+ 500 + math.random(0,500)
			timer.performWithDelay(timerdelay, AR)

		end
end


local function initializeLevel()
	physics.stop()
	physics.start()
	physics.setGravity(0,0)
	if(map~= nil) then   --clean up everything if there was a previous level
		map:destroy()

		for i = 1, runnerCount do
			runner[i]:removeSelf()
		end
		for i=1,#tower do
			for j=1, tower[i].appleCount do
				if(apple[i][j] ~=nil) then
					apple[i][j]:removeSelf()
					apple[i][j]=nil
				end
			end
			tower[i]:removeSelf()
			tower[i]=nil
		end
		towerCount = 0
		runnerCount = 0
		towerCount = 0
		appleCount = 0
		tower = nil  
		tower = {}
		towerTiles = nil
		towerTiles = {}
		apple = nil
		apple = {}
		runner = nil
		runner = {}
		waypoint= nil
		waypoint={}
		clubhouse = nil
		clubhouse = {}
	end

-- Load and build map
	local currentLevel = "Lvl"..level..".tmx"
   map = lime.loadMap(currentLevel)
   visual = lime.createVisual(map)
   physical = lime.buildPhysical(map)

   tilesObj= map:getObjectsWithName("WP")

	initializeTowers()
	for i = 1, #tilesObj, 1 do
		waypoint[i]="Waypoint"..i
	end
	for i = 1, #tilesObj, 1 do
		for j = 1, #tilesObj, 1 do
			if tilesObj[j].type== waypoint[i] then
				waypoint[i]={}
			    waypoint[i].objType = tilesObj[j].objType
				waypoint[i].x,waypoint[i].y = tilesObj[j]:getPosition()
				waypoint[i].baseTime=tilesObj[j].baseTime
			     waypoint[i].myName=tilesObj[j].myName

			     print("Waypoint "..i..": x"..waypoint[i].x.." y "..waypoint[i].y.." objType: "..waypoint[i].objType)
			end
		end
	end	

	--  Find where towers can be set
	    local TTiles = map:getTilesWithProperty("tower")
		for i = 1,#TTiles do
			towerTiles[i]={}
			towerTiles[i].row = TTiles[i].row
			towerTiles[i].column = TTiles[i].column
			towerTiles[i].tower = TTiles[i].tower
		end
    --	map:showDebugImages()  -- shows all tile objects
		clubhouse=runnerFactory:newSpriteGroup("club house100")
		 if(damage > 0 and damage < 3) then
		  	clubhouse:prepare("club house75")
		  elseif(damage < 6) then
		  	clubhouse:prepare("club house50")
		  elseif(damage < 8) then
		  	clubhouse:prepare("club house25")

		end
		clubhouse:play()
		for i = 1, #tilesObj, 1 do
				if(waypoint[i].myName=="Finish") then
					clubhouse.x=waypoint[i].x
					clubhouse.y=waypoint[i].y
				end
		end
		sndImage = display.newImage("sound.png", display.contentWidth- 75, display.contentHeight- 75)
		pauseImage = display.newImage("pause.png", display.contentWidth-75, display.contentHeight - 130)
	-- release the hounds err... runners!
		startWave(1)

--	    atrace(xinspect(runner[1]:getSpriteNames()))
end


local function gameLoop()
	if paused then
		return
	end
	for i=1,runnerCount do
		if(runner[i].myName == "down") then
			physics.removeBody(runner[i])
			runner[i].myName =nil
		else
			runner[i].baseTime = runner[i].baseTime-tick
		end
	end
	-- handle tower timeTilThrow
	for i = 1, towerCount do
		if (tower[i].attackArea.timeTilThrow > 0) then
			tower[i].attackArea.timeTilThrow = tower[i].attackArea.timeTilThrow - tick
		end
		if(tower[i].attackArea.needApple == true) then
			orchard(i)
		end
		-- handle apples that missed
		for j = 1, tower[i].appleCount do
			if(apple[i][j] ~= nil) then
		    	if(apple[i][j].active == true) then   -- is the apple in play?  If yes, then subtract tick from time to live
			    	apple[i][j].timeToLive = apple[i][j].timeToLive - tick
			  	  if (apple[i][j].timeToLive <= 0) then  -- It's been out there long enough time to go back to tower
				 	    apple[i][j]:removeSelf()
					     apple[i][j]=nil
			  	  end
		    	end
			end
		end
	end

	updateScore()
	if(levelStarted == true) then  -- need a delay or multiple waves will start before the first runner appears
	-- ready for a new wave or level??
	waveFinished=true
--	print("Wave Count: "..waveCount)
	for i = 1, #runner do
		if (runner[i].myName ~= nil) then
			waveFinished = false			return
		end
	end
	
	--Wave & Level Control
		if(waveCount == 10 and waveFinished == true and level == 1) then
			level = 2
			levelStarted = false
			runnerCount=0
			applesCollected = applesCollected + 1000  -- A little bonus for finishing the level!
			waveCount = 1
			initializeLevel()
		elseif(waveCount==10 and waveFinished == true) then
			print("You Survived!  Game Over")
			--  Add a splash win screen		
		elseif(waveFinished) then
			applesCollected = applesCollected+ (100 * waveCount)
			waveCount =waveCount +1
			levelStarted= false
			runnerCount = 0
			startWave(waveCount)
		end	
	end
end


local soundOnOff = function()

	if sound then
		sound=false
	else
		sound=true
	end
end
local function pauseGame()
	if paused then
			physics.start()
			paused = false
			for i=1,#runner do
				if(runner[i].myName ~= nil or runner[i].myName ~="down") then
					runner[i].currentTransition = transition.to(runner[i], {x= waypoint[runner[i].nextWP].x, y = waypoint[runner[i].nextWP].y, time = runner[i].baseTime})
				
					runner[i]:play()
				end
			end
	else
			physics.pause()
			paused = true
			for i=1,#runner do
				if(runner[i].myName ~= nil or runner[i].myName ~="down") then
					transition.cancel(runner[i].currentTransition)
					runner[i]:pause()
				end
			end
		
	end
	return true
end

local startGame = function()
	splash:removeSelf()
	newGameButton=nil
	loadGameButton=nil
	initializeLevel()
	initializeStatus()
	timer.performWithDelay(tick, gameLoop, 0)
	sndImage:addEventListener("tap", soundOnOff)
	pauseImage:addEventListener("tap", pauseGame)
	Runtime:addEventListener("collision", pathfinder)
end

local loadGame = function()
	-- Set location for saved data
	local filePath = system.pathForFile( "savedgame.txt", system.DocumentsDirectory )
	local file = io.open(filePath, "r")
	local saveData = ""
	saveData = file:read("*a")
	print(saveData)
	file:close()
	--break the data into usable info
	savedData = {}
--	print( string.find(saveData, ","))
	for k, v in string.gmatch(saveData, "(%w+)=(%w+)") do
		savedData[k] = v
--		print(k..", "..savedData[k])
	end	level = tonumber(savedData["level"])	waveCount = tonumber(savedData["waveCount"])	score = tonumber(savedData["score"])	applesCollected = tonumber(savedData["applesCollected"])	damage = tonumber(savedData["damage"])	print (level..", "..waveCount)	startGame()
end


local saveGame = function()
	-- Set location for saved data
	local filePath = system.pathForFile( "savedgame.txt", system.DocumentsDirectory )
	local file = io.open( filePath, "w+" )
	file:write("level="..level..", ")
	file:write("waveCount=".. waveCount..", ")
	file:write("score=".. score..", ")
	file:write("applesCollected=".. applesCollected..", ")
	file:write("damage=".. damage..", ")
	file:close()
end


local function onSystemEvent(event)
	-- handle unexpected close or interruptions
	if (event.type=="applicationExit" or event.type=="applicationSuspend") then
			--if game isn't paused, pause it
		if (not paused) then
			pauseGame()
		end
		-- save game
		saveGame()
	elseif(event.type=="applicationResume") then
		pauseGame()
	end
end


local startMenu = function()
	-- Show splash screen
	splash = display.newImage("splash.png")
	splash.x = display.contentWidth/2
	splash.y = display.contentHeight/2
	newGameButton = ui.newButton{
		default = "buttonBlue.png",
		onPress = startGame,
		text = "New Game",
		emboss = true
	}
	newGameButton.x = display.contentWidth/2
	newGameButton.y = display.contentHeight/2 - 50
	
	-- Check for old game (if one exists) 
	local filePath = system.pathForFile( "savedgame.txt", system.DocumentsDirectory )
	local file = io.open(filePath, "r")
	if file then 
		-- Game exists, so show load button
		loadGameButton = ui.newButton{
			default = "buttonBlue.png",
			onPress = loadGame,
			text = "Load Game",
			emboss = true
		}
		loadGameButton.x = display.contentWidth/2
		loadGameButton.y = display.contentHeight/2 + 50
	end	
end


startMenu()

Runtime:addEventListener( "system", onSystemEvent )
--require('loq_profiler').createProfiler()