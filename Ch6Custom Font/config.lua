-- config.lua for project: Ch6CustomFont
-- Managed with http://OutlawGameTools.com
-- Copyright 2013 Brian Burton. All Rights Reserved.
-- cpmgen config.lua
application =
{
	content =
	{
		width = 320,
		height = 480,
		scale = "letterbox",
		fps = 30,
		antialias = false,
		xalign = "center",
		yalign = "center"
	}
}