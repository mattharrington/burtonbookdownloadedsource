-- Project: Ch15as The Crow Flys
-- Description:
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
-- locations database derived from MaxMind World Cities database and is Copyright 2008 by MaxMind Inc.
--[[ Redistribution and use with or with out modifications, are premitted provided that the following conditions 
are met:
1. Redistributions must retain the above copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the distribution.
2. all advertising materials and documentation mentiong features or use of this database must display 
the the following acknowledgement:
 "This Product includes data createdby MaxMind, available from http://www.maxmind.com/" 
3. "MaxMind" may not be used to endorse or promote products derived from this database without
specific prior written permission.]]
-- Copyright 2011 Brian Burton. All Rights Reserved.
---- cpmgen main.lua

local widget = require "widget"
local sqlite = require "sqlite3"

widget.setTheme( "theme_ios" )  

local columnData = {}
local picker, selectButton, selectedCountry, selectedState, selectedCity, LongCN, LongState

-- Does the database exist in the documents directory (allows updating and persistance)
local path = system.pathForFile("locations.sqlite", system.DocumentsDirectory )
file = io.open( path, "r" )
   if( file == nil )then           
   	-- Doesn't Already Exist, So Copy it In From Resource Directory                          
   	pathSource     = system.pathForFile( "locations.sqlite", system.ResourceDirectory )  
   	fileSource = io.open( pathSource, "r" ) 
   	contentsSource = fileSource:read( "*a" )                                  
		--Write Destination File in Documents Directory                                  
		pathDest = system.pathForFile( "locations.sqlite", system.DocumentsDirectory )                 
		fileDest = io.open( pathDest, "w" )                 
		fileDest:write( contentsSource )                 
		 -- Done                      
		io.close( fileSource )        
		io.close( fileDest )         
   end   
-- One Way or Another The Database File Exists Now -- So Open Database Connection         
local db = sqlite.open( path )
print ("Locations opened")

-- handle the applicationExit event to close the db
local function onSystemEvent( event )
	if( event.type == "applicationExit") then
		db:close()
	end
end

-- load first column data from SQLite database
	   columnData[1]={}
	   countryData={}
       local count =0
       local sql = "SELECT DISTINCT country, cn FROM Country ORDER BY country ASC"  
       for row in db:nrows(sql) do
	       count = count +1
	       columnData[1][count]= row.country
	       countryData[count]=row.cn
	  --     print(columnData[1][count])	
       end
       columnData[1].alignment = "left"
       columnData[1].width = 296
       columnData[1].startIndex = 180
       columnData[1].fontSize = 12


local myCityButtonPress = function()
    local selectedColumn = picker:getValues()
    local count = 0
    local sql
    if selectedState ~= nil then
	    print(selectedColumn[3].index) -- to get the value of the selected column use .value
        selectedCity = selectedColumn[3].value
        print(selectedCity)
		sql = "SELECT * FROM lonlat WHERE country = '"..selectedCountry.."' and state = '"..selectedState.."' and city = '"..selectedCity.."' ORDER BY city ASC"  
    else
        print(selectedColumn[2].index) 
        selectedCity = selectedColumn[2].value
        print(selectedCity)
		sql = "SELECT * FROM lonlat WHERE country = '"..selectedCountry.."' and city = '"..selectedCity.."' ORDER BY city ASC"  
    end
    for row in db:nrows(sql) do
	       count = count +1
	       lon = row.lon .. "."..row.lond..row.londir
	       lat = row.lat.."."..row.latd..row.latdir
	       print("Longitude: "..lon..", Latitude: "..lat)	
     end
    display.remove(picker)
    picker=nil
    display.remove(selectButton)
    selectButton = nil
    local cn = display.newText("Country: "..LongCN, 20,20)
    cn:setTextColor(255,255,255)
    if selectedState ~= nil then
    	local st = display.newText("State: "..LongState,20, 50)
    	st:setTextColor(255,255,255)
    end
    local city = display.newText("City: "..selectedCity,20, 80)
    city:setTextColor(255,255,255)
    local long = display.newText("Longitude: "..lon,20,110)
    long:setTextColor(255,255,255)
    local lati=display.newText("Latitude: "..lat,20,140)
    lati:setTextColor(255,255,255)
end

local loadCityData = function()
	selectButton.onPress = myCityButtonPress
	local count = 0
	-- handle countries with states else all other countries
	if selectedState ~= nil then
		columnData[3]={}
		local sql = "SELECT city FROM lonlat WHERE country = '"..selectedCountry.."' and state = '"..selectedState.."' ORDER BY city ASC"  
	    for row in db:nrows(sql) do
	           count = count +1
	           columnData[3][count]= row.city
	          -- print(columnData[3][count])	
	    end
	    display.remove(picker)
        picker = nil
        columnData[1].width= 50
        columnData[2].width = 50
        columnData[2].alignment = "left"
        columnData[3].width= 196
        columnData[2].fontSize = 12
        picker = widget.newPickerWheel{
           top=208, 
           font="Helvetica-Bold",
           fontSize=16,
           columns = columnData  }
	else
		columnData[2]={}	
		local sql = "SELECT city FROM lonlat WHERE country = '"..selectedCountry.."' ORDER BY city ASC"  
        for row in db:nrows(sql) do
	       count = count +1
	       columnData[2][count]= row.city
	       print(columnData[2][count])	
        end
        display.remove(picker)
        picker = nil
        columnData[1].width = 60
        columnData[2].alignment = "left"
        columnData[2].width= 236
        picker = widget.newPickerWheel{
           top=208,
           font="Helvetica-Bold",
           fontSize=16,
           columns = columnData  }
	end
end


local myStateButtonPress = function()
        local selectedColumn = picker:getValues()
        print(selectedColumn[2].index) -- to get the value of the selected column use .value
	    LongState = selectedColumn[2].value
        selectedState = stateData[selectedColumn[2].index]
        columnData[2] = stateData
        columnData[2].startIndex=selectedColumn[2].index
        print(selectedState)
        loadCityData()
end

local loadStateData = function()
	   selectButton.onPress = myStateButtonPress
	   columnData[2]={}
	   stateData = {}
	   print(selectedCountry.." in load State Data")
	   local count =0
       local sql = "SELECT state, st FROM State WHERE country = '"..selectedCountry.."' ORDER BY state ASC"  
       for row in db:nrows(sql) do
	       count = count +1
	       columnData[2][count]= row.state
	       stateData[count]=row.st
	      --print(columnData[2][count])	
       end
       display.remove(picker)
       picker = nil
       columnData[1].width = 60
       columnData[2].alignment = "left"
       columnData[2].width= 236
       picker = widget.newPickerWheel{
           top=208,
           font="Helvetica-Bold",
           fontSize=16,
           columns = columnData  }
end

local myCountryButtonPress = function()
        local selectedColumn = picker:getValues()
        print(selectedColumn[1].index) -- to get the value of the selected column use .value
        LongCN = selectedColumn[1].value
        selectedCountry = countryData[selectedColumn[1].index]
        columnData[1] = countryData
        columnData[1].startIndex=selectedColumn[1].index
        --print(selectedCountry)
        if (selectedCountry == "US" or selectedCountry == "CA" ) then
        	loadStateData()
        else	
           loadCityData()
        end 
end

picker = widget.newPickerWheel{
        id="myPicker",
        font="Helvetica-Bold",
        fontSize=16,
        top=208,
        columns=columnData
}

selectButton = widget.newButton{
        id = "button1",
        left = 90,
        top = 100,
        label = "Select",
        width = 150, height = 28,
        cornerRadius = 8,
        onPress = myCountryButtonPress
    }




-- system listener for applicationExit
Runtime:addEventListener ("system", onSystemEvent)
