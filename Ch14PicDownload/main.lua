-- Project: Ch14 Pic Download
-- Description:
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- Copyright 2011 Brian Burton. All Rights Reserved.
---- cpmgen main.lua

local ui = require("ui")

-- Load the relevant LuaSocket modules
local http = require("socket.http")
local ltn12 = require("ltn12")
 
-- Create local file for saving data
local path = system.pathForFile( "MobileAppDevelopmentCover.png", system.DocumentsDirectory )
myFile = io.open( path, "w+b" ) 
 
 --[[local function networkListener (event)
 	if (event.isError) then
 		print("Network error - download failed")
 	else
 		print("No problems on the network side")
 		testImage = display.newImage("MobileAppDevelopmentCover.png", system.DocumentsDirectory,10,30);
         testImage.x = display.contentWidth/2
 	end
 end]]--
 
local function loadButtonPress (event)
 
   -- Request remote file and save data to local file
  http.request{
      url = "http://www.BurtonsMediaGroup.com/MobileAppDevelopmentCover.png", 
      sink = ltn12.sink.file(myFile),
   }
--network.download("http://www.BurtonsMediaGroup.com/MobileAppDevelopmentCover.png", "GET", networkListener, "MobileAppDevelopmentCover.png", system.DocumentsDirectory)
   -- Display local file
   print("should be downloaded now")
 		testImage = display.newImage("MobileAppDevelopmentCover.png", system.DocumentsDirectory,10,30);
         testImage.x = display.contentWidth/2
end

-- Load Button

loadButton = ui.newButton{

	default = "buttonBlue.png",

	over = "buttonBlueOver.png",

	onPress = loadButtonPress,

	text = "Load Picture",

	emboss = true

}
loadButton.x = display.contentWidth/2
loadButton.y = display.contentHeight - 50
