local myButton = display.newImage( "button.png" )
myButton.x = display.contentWidth /2
myButton.y = display.contentHeight -75

local textobj = display.newText("Button Tapped", 10,10,native.systemFont, 24)
textobj:setTextColor(255,255,255)

--local w = textobj.width
--local h = textobj.height

function moveButtonDown( event )
    if (textobj.y > display.contentHeight -130) then
	textobj.y = 30
    else
	textobj.y = textobj.y + 50
    end
end
 


myButton:addEventListener( "tap", moveButtonDown )