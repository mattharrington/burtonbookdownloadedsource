--
-- Project: Ch11.4 tab bar et al.
-- Description: 
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- 
-- Some graphics and code samples borrowed from Corona Labs. 
-- Set the background to white
display.setDefault( "background", 255, 255, 255 )

-- Require the widget & storyboard libraries
local widget = require( "widget" )
local storyboard = require( "storyboard" )

-- The gradient used by the title bar
local titleGradient = graphics.newGradient( 
	{ 189, 203, 220, 255 }, 
	{ 89, 116, 152, 255 }, "down" )
	
-- Create a title bar
local titleBar = display.newRect( 0, 0, display.contentWidth, 32 )
titleBar.y = titleBar.contentHeight * 0.5
titleBar:setFillColor( titleGradient )	

-- Create the title bar text
local titleBarText = display.newText( "Widget Demo", 0, 0, native.systemFontBold, 16 )
titleBarText.x = titleBar.x
titleBarText.y = titleBar.y
	
-- Create buttons table for the tab bar
local tabButtons = 
{
	{
		width = 32, 
		height = 32,
		defaultFile = "tabIcon.png",
		overFile = "tabIcon-down.png",
		label = "Segemented",
		onPress = function() storyboard.gotoScene( "tab1" ); end,
		selected = true
	},
	{
		width = 32, 
		height = 32,
		defaultFile = "tabIcon.png",
		overFile = "tabIcon-down.png",
		label = "ScrollView",
		onPress = function() storyboard.gotoScene( "tab2" ); end,
	},
	{
		width = 32, 
		height = 32,
		defaultFile = "tabIcon.png",
		overFile = "tabIcon-down.png",
		label = "Other",
		onPress = function() storyboard.gotoScene( "tab3" ); end,
	}
}

-- Create a tab-bar and place it at the bottom of the screen
local tabBar = widget.newTabBar
{
	top = display.contentHeight - 50,
	width = display.contentWidth,
	buttons = tabButtons
}

-- Start at tab1
storyboard.gotoScene( "tab1" )