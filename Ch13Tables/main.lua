-- Project: Ch13 Tables
-- Description:
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
---- cpmgen main.lua
display.setStatusBar( display.HiddenStatusBar ) 

local ui = require("ui")
local tableView = require("tableView")

local myList, backBtn, detailScreenText
local screenOffsetW, screenOffsetH = display.contentWidth -  display.viewableContentWidth, display.contentHeight - display.viewableContentHeight


-- load the array to be displayed in the table view
local data = {}
-- set each row of the array as an array, then load state name and abbreviation
data[1] = {}
data[1].state ="California"
data[1].abbrev="CA"

data[2] = {}
data[2].state ="Indiana"
data[2].abbrev="IN"

data[3] = {}
data[3].state ="Massachusetts"
data[3].abbrev="MA"

data[4] = {}
data[4].state ="Missouri"
data[4].abbrev="MO"

data[5] = {}
data[5].state ="Texas"
data[5].abbrev="TX"

--setup a destination for the list items
local detailScreen = display.newGroup()

local detailBg = display.newRect(0,0,display.contentWidth,display.contentHeight-display.screenOriginY)
detailBg:setFillColor(255,255,255)
detailScreen:insert(detailBg)

detailScreenText = display.newText("You tapped item", 0, 0, native.systemFontBold, 16)
detailScreenText:setTextColor(0, 0, 0)
detailScreen:insert(detailScreenText)
detailScreenText.x = math.floor(display.contentWidth/2)
detailScreenText.y = math.floor(display.contentHeight/2) 	
detailScreen.x = display.contentWidth

--setup functions to execute on touch of the list view items
function listButtonRelease( event )
	self = event.target
	local id = self.id
	print(self.id)
	
	detailScreenText.text = "The abbrev. for ".. data[self.id].state .." is "..data[self.id].abbrev
			
	transition.to(myList, {time=400, x=display.contentWidth*-1, transition=easing.outExpo })
	transition.to(detailScreen, {time=400, x=0, transition=easing.outExpo })
	transition.to(backBtn, {time=400, x=math.floor(backBtn.width/2) + screenOffsetW*.5 + 6, transition=easing.outExpo })
	transition.to(backBtn, {time=400, alpha=1 })
	
	delta, velocity = 0, 0
end

function backBtnRelease( event )
	print("back button released")
	transition.to(myList, {time=400, x=0, transition=easing.outExpo })
	transition.to(detailScreen, {time=400, x=display.contentWidth, transition=easing.outExpo })
	transition.to(backBtn, {time=400, x=math.floor(backBtn.width/2)+backBtn.width, transition=easing.outExpo })
	transition.to(backBtn, {time=400, alpha=0 })

	delta, velocity = 0, 0
end

myList = tableView.newList{
        data=data,
        default="listItemBg.png",
        over="listItemBg_over.png",
        onRelease=listButtonRelease,
        top=60,
        bottom=1,
        callback=function(item) 
                        local t = display.newText(item.state, 0, 0, native.systemFontBold, textSize)
                        t:setTextColor(255, 255, 255)
                        t.x = math.floor(t.width/2) + 20
                        t.y = 46 
                        return t
                end
}

myList:addScrollBar()

--Setup the nav bar 
local navBar = ui.newButton{
	default = "navBar.png",
	onRelease = scrollToTop
}
navBar.x = display.contentWidth*.5
navBar.y = math.floor(display.screenOriginY + navBar.height*0.5)

local navHeader = display.newText("Places I've Lived", 0, 0, native.systemFontBold, 16)
navHeader:setTextColor(255, 255, 255)
navHeader.x = display.contentWidth*.5
navHeader.y = navBar.y

--Setup the back button
backBtn = ui.newButton{ 
	default = "backButton.png", 
	over = "backButton_over.png", 
	onRelease = backBtnRelease
}
backBtn.x = math.floor(backBtn.width/2) + backBtn.width + screenOffsetW
backBtn.y = navBar.y 
backBtn.alpha = 0

