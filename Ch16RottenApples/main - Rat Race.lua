-- Project: Ch16 Rotten Apples
-- Managed with http://CoronaProjectManager.com
--
-- Copyright 2011 Brian Burton. All Rights Reserved.
--local director = require("director")

display.setStatusBar(display.HiddenStatusBar)
local loqsprite = require("loq_sprite")
local lime = require("lime")
local physics = require("physics")
physics.start()
physics.setGravity(0,0)
local runnerFactory = loqsprite.newFactory("runnersheet") -- load spriteLoq sheet

-- A few variables to keep track of game elements
local start={}		 	-- map starting point
local runner = {} 	  -- array for tracking runnerslocal waypoint = {}  -- array for tracking waypoints
--[[
local tower = {}  	   -- array for tracking towers
local clubhouse = {}  -- array for tracking status of clubhouse
local tick                     -- game speed
]]

--[[   Game parameters
	runner[].speed  -- the speed of the runner (1 - normal, 2 - twice as fast)
	runner[].hp  -- base hitpoints (how many hits to disable)
	runner[].goal -- which waypoint is next
	
thrower[].speed - how quickly the tower can throw again
	thrower[].range - current range of tower
	thrower[].damage - amount of damage
	thrower.x, thrower.y - tower location
	
	level.tick - how quickly to do a new frame
	level.apples - base number of apples to start level
	level.	
	]]

local function pathfinder(event)
    print("Pathfinder was called")
	local myObj = {}
	local WPHit = {}	if(event.object1.myName=="Runner") then
		myObj = event.object1		WPHit = event.object2
	elseif (event.object2.myName=="Runner") then
		myObj = event.object2		WPHit = event.object1
	else
		return
	end     local wpgoal = "Waypoint"..myObj.nextWP     if(wpgoal ~= WPHit.myName) then	     return     end	

     print("waypoint goal: "..myObj.nextWP.." WP Hit: "..WPHit.myName)
	if (myObj.nextWP+1 ~= nil) then

		myObj.nextWP = myObj.nextWP+1
		if (waypoint[myObj.nextWP-1].x < waypoint[myObj.nextWP].x) then
			print("Load Runner right")			transition.cancel(myObj)			myObj:prepare("red runner right")			myObj:play()
			transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = waypoint[myObj.nextWP-1].baseTime})
 		elseif (waypoint[myObj.nextWP-1].x > waypoint[myObj.nextWP].x) then
		    print("Load Runner left")		    transition.cancel(myObj)
			myObj:prepare("red runner left")			myObj:play()			transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = waypoint[myObj.nextWP-1].baseTime})
         elseif (waypoint[myObj.nextWP-1].y < waypoint[myObj.nextWP].y) then
           print("Load Runner down")            transition.cancel(myObj)            myObj:prepare("red runner down")			myObj:play()
			transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = waypoint[myObj.nextWP-1].baseTime})
		 elseif (waypoint[myObj.nextWP-1].y > waypoint[myObj.nextWP].y) then
		   print("Load Runner up")
			 transition.cancel(myObj)			myObj:prepare("red runner up")			myObj:play()			transition.to(myObj, {x= waypoint[myObj.nextWP].x-16, y = waypoint[myObj.nextWP].y-16, time = waypoint[myObj.nextWP-1].baseTime})
		end
	else
		-- clubhouse is goal
	end
end


-- Load and build map
local map = lime.loadMap("Lvl1.tmx")
local visual = lime.createVisual(map)
local physical = lime.buildPhysical(map)

local tilesObj= map:getObjectsWithName("WP")

	for i = 1, #tilesObj, 1 do
		waypoint[i]="Waypoint"..i
	end
	for i = 1, #tilesObj, 1 do
		for j = 1, #tilesObj, 1 do
			if tilesObj[j].type== waypoint[i] then
				waypoint[i]={}
				waypoint[i].x,waypoint[i].y = tilesObj[j]:getPosition()
				waypoint[i].baseTime=tilesObj[j].baseTime
			     waypoint[i].myName=tilesObj[j].myName
			     print("Waypoint "..i..": x"..waypoint[i].x.." y "..waypoint[i].y)
			end
		end	

	end

--Create Runner
--local runner[1] ={}
runner[1] = runnerFactory:newSpriteGroup("red runner up")
runner[1].x=waypoint[1].x-16
runner[1].y=waypoint[1].y-32
runner[1].speed = 1
runner[1].nextWP = 2
runner[1].hp = 3
runner[1].myName="Runner"
runner[1]:play()

physics.addBody(runner[1])
transition.to(runner[1], {x= waypoint[2].x-16, y = waypoint[2].y-16, time = waypoint[1].baseTime})


--atrace(xinspect(runner[1]:getSpriteNames()))


local update = function( event )
	map:update( event )
end

Runtime:addEventListener( "enterFrame", update )
Runtime:addEventListener("collision", pathfinder)

--require('loq_profiler').createProfiler()