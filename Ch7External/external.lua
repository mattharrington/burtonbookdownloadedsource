--
-- Project: Ch7 External
-- Description: 
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- Copyright 2011 Brian Burton. All Rights Reserved.
-- 
module(..., package.seeall)

function hiDad()
	textObj = display.newText("Hi Dad", display.contentWidth/2, display.contentHeight/2, native.systemFont, 24)
	textObj:setTextColor(255,255,255)
end
