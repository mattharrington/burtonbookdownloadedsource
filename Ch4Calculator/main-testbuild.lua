-- Created by: Dr. Brian Burton, Burton's Media Group.
-- All Rights Reserved
-- Project 4.1 Calculator

-- load buttons and place in on the display
local width = display.contentWidth/2
local height = display.contentHeight/2 - 100
local addButton = display.newImage("add.png", width -150, height)
local subtractButton = display.newImage("subtract.png", width -50, height)
local multiplyButton = display.newImage("multiply.png", width +50, height)
local divideButton = display.newImage("divide.png", width + 150, height)
local equalButton = display.newImage("equals.png", width, height + 250)

-- Textbox for first number
local firstNumber = 10
local firstNumberText = display.newText(firstNumber, width + 100, height - 150, native.systemFont, 36)

-- Textbox for second number
local secondNumber = 5
local secondNumberText = display.newText(secondNumber, width + 100, height +150, native.systemFont, 36)
local operator   -- variable to tell us which operator was selected
local result     -- variable to hold the result
local operandSelected = "False"
local resultText
local warningText

local function addButtonTap(event)
	operator = "+"
	operandSelected = "True"

end
local function subtractButtonTap(event)
	operator = "-"
	operandSelected = "True"
end
local function multiplyButtonTap(event)
	operator = "*"
	operandSelected = "True"
end
local function divideButtonTap(event)
	operator = "/"
	operandSelected = "True"
end


local function equalButtonTap(event) 	
  if operandSelected == "True" then
	if operator == "+" then
		result = firstNumber + secondNumber
	elseif operator == "-" then
		result = firstNumber - secondNumber
	elseif operator == "*" then
		result = firstNumber * secondNumber
	elseif operator == "/" then
		result = firstNumber / secondNumber
	end
	display.remove(resultText)
	resultText = display.newText(result, width + 100, height +350, native.systemFont, 36)
  	operandSelected = "False"
  else
  	local warningText = display.newText("Select operation first", width-200, 50, native.systemFont, 36)
  end	
end	


addButton:addEventListener("tap", addButtonTap)
subtractButton:addEventListener("tap", subtractButtonTap)
multiplyButton:addEventListener("tap", multiplyButtonTap)
divideButton:addEventListener("tap", divideButtonTap)
equalButton:addEventListener("tap", equalButtonTap)