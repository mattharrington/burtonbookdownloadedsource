--
-- Project: tabIcon-down.png
-- Description: 
--
-- Version: 1.0
-- Managed with http://CoronaProjectManager.com
--
-- 
-- 
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- Our scene
function scene:createScene( event )
	local group = self.view
	
	-- Display a background
	local background = display.newImage( "background.png", true )
	group:insert( background )
	
	-- Our ScrollView listener
	local function scrollListener( event )
		local phase = event.phase
		local direction = event.direction
		
		if "began" == phase then
			--print( "Began" )
		elseif "moved" == phase then
			--print( "Moved" )
		elseif "ended" == phase then
			--print( "Ended" )
		end
		
		-- If the scrollView has reached it's scroll limit
		if event.limitReached then
			if "up" == direction then
				print( "Reached Top Limit" )
			elseif "down" == direction then
				print( "Reached Bottom Limit" )
			elseif "left" == direction then
				print( "Reached Left Limit" )
			elseif "right" == direction then
				print( "Reached Right Limit" )
			end
		end
				
		return true
	end

	-- Create a ScrollView
	local scrollView = widget.newScrollView
	{
		left = 10,
		top = 52,
		width = 300,
		height = 350,
		id = "onBottom",
		hideBackground = true,
		horizontalScrollingDisabled = false,
		verticalScrollingDisabled = false,
		maskFile = "scrollViewMask-350.png",
		listener = scrollListener,
	}
	
	-- Insert an image into the scrollView
	local background = display.newImageRect( "scrollimage.jpg", 768, 1024 )
	background.x = background.contentWidth * 0.5
	background.y = background.contentHeight * 0.5
	scrollView:insert( background )
	group:insert( scrollView )
end

scene:addEventListener( "createScene" )

return scene